
@extends('layouts.app')
@push('header-scripts')
    @livewireStyles
@endpush

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <p>Welcome to my website</p>
                @livewire('counter')
            </div>
        </div>
    </div>
    @livewireScripts
@endsection
